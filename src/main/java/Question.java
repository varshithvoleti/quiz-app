import java.util.Scanner;
public class Question {
	String question;
	String options[] = new String[4];
	String answer;
	public void setQuestion(String question){
		this.question = question;
	}
	public void setOptions(String[] opts){
		for(int i = 0;i<opts.length;i++){
			this.options[i] = opts[i];
		}
	}
	public void setAnswer(String answer){
		this.answer = answer;
	}
	public String getQuestion() {
		return this.question;
	}
	public String[] getOptions() {
		return this.options;
	}
	public String getAnswer() {
		return this.answer;
	}
	public Question question(){
		Scanner sc = Select.sc;
		System.out.println("Enter the question");
		this.question = sc.nextLine();
		for(int i = 0;i<4;i++){
			System.out.println("Enter the option "+(i+1));
			this.options[i] = sc.nextLine();
		}
		System.out.println("Enter the correct option from 1 to 4");
		this.answer = sc.next();
		return this;
	}	
}
