import org.junit.*;
import org.junit.Assert;
public class SelectTest {
	private Question q1 = new Question();
	private String arr[] = {"2","3","4","5"};
	private Quiz q = new Quiz();
	@Before
	public void initializeQuestions() {
		this.q.setNoOfQuestions(1);
		q1.setQuestion("1+1=?");
		q1.setOptions(this.arr);
		q1.setAnswer("1");
		this.q.insertInto(q1);
	}
	@Test
	public void unittQuestionClass() {
		Assert.assertEquals("1+1=?", q1.getQuestion());
		Assert.assertArrayEquals(arr,q1.getOptions());
		Assert.assertEquals("1", q1.getAnswer());
		Assert.assertEquals(1, q.getNoOfQuestions());
	}
	@Test
	public void unittQuizClass() {
		Assert.assertEquals(".json", q.getValue("extension"));
		Assert.assertEquals("C:\\users\\hp\\desktop\\Quiz\\", q.getValue("file_location"));
	}
}