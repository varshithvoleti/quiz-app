import java.io.IOException;
import java.util.Scanner;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
public class Select {
	static Scanner sc = new Scanner(System.in);
	static Logger log = LoggerFactory.getLogger(Select.class);
	public static void main(String args[]) throws IOException{
		int n;
		Quiz quiz_obj = new Quiz();
		quiz_obj.start();
		for(int i = 0;i<50;i++) {
			System.out.println("Enter 1 to save the questions");
			System.out.println("Enter 2 to play the quiz");
			System.out.println("Enter 3 to export the quiz questions and answers");
			System.out.println("Enter 4 to import a quiz");
			System.out.println("other number to quit the quiz");
			try {
				n= sc.nextInt();
			}catch(Exception e) {
				log.info("Enter a valid number");
				continue;
			}
			if(n==1) {
				quiz_obj = new Quiz();
				sc.nextLine();
				System.out.println("Enter name of the quiz");
				quiz_obj.quiz_name = sc.nextLine();
				System.out.println("Enter no of questions");
				quiz_obj.no_of_questions = sc.nextInt();
				sc.nextLine();
				
				for(int j = 0;j<quiz_obj.no_of_questions;j++){
					Question q = new Question();
					quiz_obj.insertInto(q.question());
				}
				
			}
			if(n==2) {
				try {
				quiz_obj.playQuiz();					
				}catch(Exception e) {
					// log.info(e.getMessage());
					log.info("First enter the questions or import the quiz");
					System.out.println("First enter the questions or import the quiz");
				}
			}
			if(n==3) {
				try{
					if(quiz_obj.quiz_name==null||quiz_obj==null){
						log.info("First save the questions or import the quiz");
						System.out.println("First save the questions or import the quiz");
						continue;
					}
				}catch(Exception e){
					log.info("Nothing to export");
				}
				quiz_obj.exportQuiz();
					
			}
			if(n==4) {
				System.out.println("Enter the file location");
				sc.nextLine();
				Quiz q = new Quiz();
				String file_location = sc.nextLine();
				quiz_obj=q.importQuiz(file_location);
			}
		}
	}
}