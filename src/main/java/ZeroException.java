public class ZeroException extends ArithmeticException {
	ZeroException(String msg){
		super(msg);
	}
}
