import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Scanner;
import java.util.SortedMap;
import java.util.TreeMap;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Reader;
import java.nio.file.Files;
import java.nio.file.Paths;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVPrinter;
import org.apache.commons.csv.CSVRecord;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONObject;
import org.json.JSONTokener;
import java.nio.file.FileSystems;
import java.nio.file.Path;
import java.nio.file.StandardWatchEventKinds;
import java.nio.file.WatchEvent;
import java.nio.file.WatchKey;
import java.nio.file.WatchService;
public class Quiz extends Thread{
	 static Logger log = LogManager.getLogger(Quiz.class);
	Quiz(){
		this.loadProperties();
	}
	List<Question> questions = new ArrayList<Question>();
	static HashMap<String,String> properties = new HashMap<String,String>();
	String quiz_name;
	int no_of_questions;
	public void insertInto(Question question_obj) {
		this.questions.add(question_obj);
	}
	public void setQuizName(String name) {
		this.quiz_name = name;
	}
	public void setNoOfQuestions(int q_count) {
		this.no_of_questions = q_count;
	}
	public int getNoOfQuestions() {
		return this.no_of_questions;
	}
	public void playQuiz() {
		int players;
		Scanner sc = Select.sc;
		System.out.println("You are playing "+this.quiz_name);
		System.out.println("Enter no of players");
		players = sc.nextInt();
		if(players<=0) {
			throw new ZeroException("Atleast one player should play the game");
		}
		String names[] = new String[players];
		Map<String,Integer> hm = new HashMap<String,Integer>();
		for(int i = 0;i<players;i++) {
			System.out.println("Enter Player "+(i+1)+" name");
			names[i] = sc.next();
			hm.put(names[i],0);
		}
		String option;
		for(int i = 0;i<this.no_of_questions;i++) {
			this.displayQuestion(this.questions.get(i));
			SortedMap<Long, String> sm = new TreeMap<Long, String>();
			for(int j = 0;j<players;j++) {
				System.out.println(names[j]+" Enter your option");
				Calendar calendar1 = Calendar.getInstance();
				Date startDate = calendar1.getTime();
				option = sc.next();
				Calendar calendar = Calendar.getInstance();
				Date endDate = calendar.getTime();
				long totaltime = endDate.getTime() - startDate.getTime();
				if(option.equals(questions.get(i).answer)) {
					sm.put(totaltime, names[j]);
				}
			}
			int count = players;
			for(Long l:sm.keySet()) {
				hm.replace(sm.get(l), hm.get(sm.get(l))+count);
				count = count - 1;
			}
			System.out.printf("\nThe correct option is %s\n",questions.get(i).answer);			
		}
		Iterator<Map.Entry<String, Integer>> itr = hm.entrySet().iterator();
		System.out.println("****************Score****************");
        while(itr.hasNext())
        {
             Map.Entry<String, Integer> entry = itr.next();
             System.out.println(entry.getKey()+" Scored "+entry.getValue()+" Points");
        }
		System.out.println();
	}
	public void displayQuestion(Question q){
		System.out.println("Your questions is");
		System.out.println(q.question);
		for(int i = 0;i<4;i++){
			System.out.print("Option "+(i+1));
			System.out.println(" "+q.options[i]);
		}
	}
	public void exportQuiz() throws IOException{
		String file_extension = Quiz.properties.get("extension");
		String file_location = Quiz.properties.get("file_location")+this.quiz_name+Quiz.properties.get("extension");
		if(file_extension.equals(".csv")){
			this.exportToCSVFile(file_location);
		}else if(file_extension.equals(".json")){
			this.exportToJSONFile(file_location);
		}
		System.out.println("Successfully exported the quiz to this location "+file_location);
	}
	private void exportToJSONFile(String file_location) throws IOException {
		JSONArray all_questions = new JSONArray();
		for(int i = 0;i<this.no_of_questions;i++){
			Question question_obj = this.questions.get(i);
			JSONObject question = new JSONObject();
			question.put("Question",question_obj.question);
			question.put("Answer",question_obj.answer);
			String options[] = new String[4];
			for(int j = 0;j<4;j++){
				options[j] = question_obj.options[j];
			}
			JSONArray  json_options = new JSONArray(options);
			question.put("Options",json_options);
			all_questions.put(question);
		}
		try (FileWriter file = new FileWriter(file_location,false)) {
            file.write(all_questions.toString()); 
            file.flush();
		}
	}
	private void exportToCSVFile(String file_location) throws IOException {
		try(BufferedWriter writer = Files.newBufferedWriter(Paths.get(file_location));
        CSVPrinter csvPrinter = new CSVPrinter(writer, CSVFormat.DEFAULT)){
			for(int i = 0;i<this.no_of_questions;i++){
				Question q_obj = this.questions.get(i);
				String info[] = new String[6];
				info[0] = q_obj.question;
				for(int j = 0;j<4;j++){
					info[j+1]=q_obj.options[j];
				}
				info[5] = q_obj.answer;
				csvPrinter.printRecord(Arrays.asList(info));
			}
			csvPrinter.close();
			writer.close();
		}
	}
	public Quiz importQuiz(String file_name) throws IOException{
		this.setQuizName(file_name);
		String file_extension = Quiz.properties.get("extension");
		String file_location = Quiz.properties.get("file_location")+file_name+Quiz.properties.get("extension");
		try{
			if(file_extension.equals(".csv")){
				this.importCSVFileQuiz(file_location);
			}else if(file_extension.equals(".json")){
				this.importJSONFileQuiz(file_location);
			}
			return this;
		}catch(Exception e){
			e.printStackTrace();
			System.out.println("No such file exists");
			return null;
		}
	}
	private void importJSONFileQuiz(String file_location) throws FileNotFoundException, IOException {
		try {
	    	String total_file = Quiz.readFileAsString(file_location);
			if(total_file!=null){
				JSONArray json_arr_obj = new JSONArray(total_file);
				this.setNoOfQuestions(json_arr_obj.length());
				for(int i = 0;i<json_arr_obj.length();i++) {
					Question q = new Question();
					JSONTokener json_token = new JSONTokener(json_arr_obj.get(i).toString());
					JSONObject json_obj = new JSONObject(json_token);
					q.setQuestion((String)json_obj.get("Question"));
					String options[] = new String[4];
					JSONArray json_options = new JSONArray(json_obj.get("Options").toString());
					for(int j = 0;j<4;j++) {
						options[j] = (String)json_options.get(j);
					}
					q.setOptions(options);
					q.setAnswer((String)json_obj.get("Answer"));
					this.questions.add(q);
				}
				System.out.println("Successfully imported the file "+file_location);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	public static String readFileAsString(String file)
    {
		try{
			return new String(Files.readAllBytes(Paths.get(file)));
		}catch(Exception e){
			System.out.println("No such file exists");
			return null;
		}
    }
	private void importCSVFileQuiz(String file_location) throws IOException {
		Reader reader = Files.newBufferedReader(Paths.get(file_location));
		try(CSVParser csvParser = new CSVParser(reader, CSVFormat.newFormat(','))){
			int count = 0;
			for(CSVRecord csvrecord:csvParser){
				Question q = new Question();
				q.setQuestion(csvrecord.get(0));
				String options[] = new String[4];
				for(int i = 1;i<5;i++){
					options[i-1] = csvrecord.get(i);
				}
				q.setOptions(options);
				q.setAnswer(csvrecord.get(5));
				this.insertInto(q);
				count = count + 1;
			}
			this.setNoOfQuestions(count);
			System.out.println("Successfully imported the file "+file_location);	
		}
	}
	public void run(){
		final Path path = FileSystems.getDefault().getPath(System.getProperty("user.dir"),"resources");
		System.out.println(path);
		    try {
				final WatchService watchService = FileSystems.getDefault().newWatchService();
				final WatchKey watchKey = path.register(watchService, StandardWatchEventKinds.ENTRY_MODIFY);
				while (true) {
					final WatchKey wk = watchService.take();
						for(WatchEvent<?> event : wk.pollEvents()) {
							final Path changed = (Path) event.context();
							System.out.println(changed);
							if (changed.endsWith("info.properties")) {
								System.out.println(Paths.get(path.toString(), "info.properties").toString());
								System.out.println("file modified");
								this.loadProperties();
							}
						}
				}
			}catch (IOException e) {
				e.printStackTrace();
			}catch (InterruptedException e) {
					e.printStackTrace();
				}
		    }
		// File prop_file = new File("info.properties");
		// long last_modified = prop_file.lastModified();
		// while(true){
		// 	if(last_modified!=prop_file.lastModified()){
		// 		System.out.println("file is modified");
		// 		this.loadProperties();
		// 		last_modified = prop_file.lastModified();
		// 	}
		// }
	private void loadProperties(){
		try {
			FileReader fr = new FileReader("resources\\info.properties");
			File f = new File("resources\\info.properties");
			Properties p = new Properties();
			p.load(fr);
			Enumeration<String> enums = (Enumeration<String>) p.propertyNames();
			while (enums.hasMoreElements()) {
			  String key = enums.nextElement();
			  String value = p.getProperty(key);
			  Quiz.properties.put(key,value);
			}
		} catch (FileNotFoundException e) {
			log.warn("File not found");
			e.printStackTrace();
		}catch(IOException e){
			e.printStackTrace();
		}
	}
	public String getValue(String key) {
		return this.properties.get(key);
	}
}
